setwd("~/��� �/����� �/����� ���/datafiles")
spam.raw <- read.csv("text_sms.csv", stringsAsFactors = FALSE)

str(spam.raw)

spam<-spam.raw

spam$type<-as.factor(spam$type)

str(spam)

library(ggplot2)

ggplot(spam, aes(type)) + geom_bar()

head(spam)

#install.packages('tm')
library(tm)

spam.corpus<- Corpus(VectorSource(spam$text))

spam.corpus[[1]][[1]]
spam.corpus[[1]][[2]]

clean.corpus <- tm_map(spam.corpus, removePunctuation)
clean.corpus[[1]][[1]]

clean.corpus <- tm_map(clean.corpus, removeNumbers)
clean.corpus <- tm_map(clean.corpus, content_transformer(tolower))
clean.corpus[[1]][[1]]

clean.corpus <- tm_map(clean.corpus, removeWords, stopwords())
clean.corpus <- tm_map(clean.corpus, stripWhitespace)
clean.corpus[[1]][[1]]

dtm<-DocumentTermMatrix(clean.corpus)
dim(dtm)

dtm.freq<-DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm, 12)))
dim(dtm.freq)

inspect(dtm.freq[1:10, 1:20])

conv_01 <- function(x){
  x <- ifelse(x>0,1,0)
  return (as.integer(x))
}

dtm.final <- apply(dtm.freq, MARGIN = 1:2, conv_01)
dtm.df <- as.data.frame(dtm.final)

conv_01_type <- function(x){
  if(x == 'ham')return(as.integer(0))
  return (as.integer(1))
}

spam$type <- sapply(spam$type, conv_01_type)

dtm.df$type <- spam$type

str(dtm.df)



#Homework 9

#split to test and train

library(caTools)

filter <- sample.split(dtm.df$type, SplitRatio = 0.7)

text.train <- subset(dtm.df, filter == TRUE)
text.test <- subset(dtm.df, filter == F)

dim(dtm.df)
dim(text.train)
dim(text.test)

text.model <- glm(type ~., family = binomial(link = "logit"), data = text.train)
summary(text.model)

predicted.text.test <- predict(text.model, text.test, type = 'response')

hist(predicted.text.test)

#confusion matrix

confusion_matrix <- table(text.test$type, predicted.text.test>0.5)

#precision
precision <- confusion_matrix[2,2]/(confusion_matrix[2,2]+confusion_matrix[1,2])

#reccal
reccal<-confusion_matrix[2,2]/(confusion_matrix[2,2]+confusion_matrix[2,1])

#accuracy <- (confusion_matrix[1,1]+confusion_matrix[2,2])/length(text.test$type)

minCost<-function(df,dfc,model,tn,fy){
  predicted <- predict(model, newdata = df, type = 'response')
  decision<-min(predicted)
  confusuon_matrix <- table(dfc, predicted>decision)
  min=(confusuon_matrix[1,2])*tn+(confusuon_matrix[2,1])*fy
  while (decision<max(predicted)) {
    confusuon_matrix <- table(dfc, predicted>decision)
    temp=confusuon_matrix[1,2]*tn+confusuon_matrix[2,1]*fy
    if(temp<min){
      min<-temp
      f.d<-decision}
    decision<-decision+0.001}
  re<-c(f.d,min)
  return(re)}

T.N<-5
F.Y<-1
decision.cost<-minCost(text.test,text.test$type,text.model,T.N,F.Y)

#lesson 11

#install.packages('ROSE')
library(ROSE)

text.train.over <- ovun.sample(type ~., data = text.train, method = 'over', N = 600)$data

table(text.train.over$type)
ggplot(text.train.over, aes(type)) + geom_bar()

text.model.over <- glm(type ~., family = binomial(link = "logit"), data = text.train.over)
summary(text.model.over)

predicted.over <- predict(text.model.over, text.test, type = 'response')

hist(predicted.over)

#confusion matrix

confusion_matrix.over <- table(text.test$type, predicted.over>0.5)

#precision
precision <- confusion_matrix.over[2,2]/(confusion_matrix.over[2,2]+confusion_matrix.over[1,2])

#reccal
reccal<-confusion_matrix.over[2,2]/(confusion_matrix.over[2,2]+confusion_matrix.over[2,1])


dim(dtm)


