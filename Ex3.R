df.raw<- read.csv(url('https://raw.githubusercontent.com/datasets/covid-19/master/data/countries-aggregated.csv'))

str(df.raw)

df <- df.raw
df$Date<-as.Date(df$Date)
str(df)

df$Active<-df$Confirmed-df$Recovered-df$Deaths
df$Mort.rate<-df$Deaths/(df$Confirmed+1)

#1

df.germany<- df %>% filter(Country == 'Germany')
df.italy<- df %>% filter(Country == 'Italy')

ggplot() + 
  geom_point(data = df.italy, aes(Date, Confirmed), color = 'green' ) +
  geom_point(data = df.germany, aes(Date, Confirmed), color = 'black' )

#2

df.israel<- df %>% filter(Country == 'Israel')
df.unitedkingdom<- df %>% filter(Country == 'United Kingdom')

min.conf<-df %>% filter(Confirmed>=10)
min.conf.united.Kingdom<- min.conf %>% filter(Country == 'United Kingdom')
min.conf.israel<- min.conf %>% filter(Country == 'Israel')

ggplot() + 
  geom_point(data = min.conf.united.Kingdom, aes(Date, Confirmed), color = 'green' ) +
  geom_point(data = min.conf.israel, aes(Date, Confirmed), color = 'blue')

##another option - Roni's

gt.ten <- function(x){
  if(x>= 10) return (TRUE)
  return (FALSE)
}

filter.isr <- sapply(df.israel$Confirmed, gt.ten)
filter.uk <- sapply(df.unitedkingdom$Confirmed, gt.ten)

df.isr.from10 <- df.israel[filter.isr,]
df.uk.from10 <- df.unitedkingdom[filter.uk,]

days.isr <- 1:nrow(df.isr.from10)
days.uk <- 1:nrow(df.uk.from10)

df.isr.from10$days <- NULL
df.uk.from10$days <- NULL

ggplot() + 
  geom_point(data = df.uk.from10, aes(Date, Confirmed), color = 'green' ) +
  geom_point(data = df.isr.from10, aes(Date, Confirmed), color = 'blue')


#3

lag <- function (date, country, df, days){
  day<-date-days
  if(day>=df$Date[1]){
    v<- df %>% filter(Date==day, Country==country)
    return(v$Confirmed)
  } 
  else{
    return(0)
  }
}


confirmed.lag <- mapply(lag, df$Date, df$Country, MoreArgs = list(df,10))

df$Confirmed.lag <- confirmed.lag
df$Mort.rate.fixed <- df$Deaths/(df$Confirmed.lag + 1)

df.unitedkingdom<- df %>% filter(Country == 'United Kingdom')

ggplot() + 
  geom_point(data = df.italy, aes(Date, Mort.rate.fixed), color = 'green' ) +
  geom_point(data = df.germany, aes(Date, Mort.rate.fixed), color = 'black' )+
  geom_point(data = df.unitedkingdom, aes(Date, Mort.rate.fixed), color = 'red' )


