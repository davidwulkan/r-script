df.raw<- read.csv(url('https://raw.githubusercontent.com/datasets/covid-19/master/data/countries-aggregated.csv'))
setwd("~/��� �/����� �/����� ���/datafiles")

str(df.raw)

df <- df.raw
df$Date<-as.Date(df$Date)
str(df)

df$Active<-df$Confirmed-df$Recovered-df$Deaths
df$Mort.rate<-df$Deaths/(df$Confirmed+1)

countries <- df %>% distinct(Country)

interesting.countries <- c('Tanzania', 'France', 'Kenya', 'South Africa', 'Italy', 'Uganda', 'Spain', 'Colombia', 'Thailand', 'United Kingdom')

df.int.countries <- df %>% filter (Country %in% interesting.countries)

today <- as.Date('2020-05-03')

df.int.countries.today <- df.int.countries %>% filter (Date == today)

#show my table titles 
str(df.int.countries.today)

#write.csv(df.int.countries.today, file ='ExCorona-03-05.csv')
countries.Details <- read.csv('Excorona-03-05.csv')

str(countries.Details)

ggplot(countries.Details, aes(x = Latitude, y = Mort.rate)) + 
  geom_point(aes(color = Country, size = Age), alpha = 0.5) +
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07","darkolivegreen3","deepskyblue4","darkkhaki","brown1","aquamarine","bisque","chocolate","cadetblue","#00AFBB"
)) +
  scale_size(range = c(0.5, 12))  # Adjust the range of points size

